// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cross_platform_test/drawer.dart';
import 'package:cross_platform_test/fonts.dart';
import 'package:flutter/material.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:cross_platform_test/sizes.dart';
import 'package:wifi_iot/wifi_iot.dart';
import 'colors.dart';

void main() {
  runApp(
    const MaterialApp(
      home: AppScreen(),
    ),
  );
}

class AppScreen extends StatefulWidget {
  const AppScreen({Key? key}) : super(key: key);

  @override
  HomeScreen createState() => HomeScreen();
}

class HomeScreen extends State<AppScreen> {
  bool _isConnected = false;
  int count = 0;
  bool isDone = true;
  String bssid = 'null1';
  String? ip;
  int fileNum = 0;
  int fileCount = 0;
  List<int> fileNums = [];
  String path = '';
  final _messengerKey = GlobalKey<ScaffoldMessengerState>();

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> _localFile(int fileNum) async {
    return File('$path/wibl-raw.$fileNum.wibl');
  }

  void createFile(Uint8List data, int number) async {
    File f = await _localFile(number);
    f.writeAsBytes(data);
  }

  Future<bool> _createFolder(String ssid) async {
    path = await _localPath;
    Directory d = Directory(path + "/$ssid");
    if (!(await d.exists())) {
      d.create();
      print('Created ' + d.path);
    }
    path = d.path;
    return Future<bool>(() {
      return true;
    });
  }

  Future<bool> setId() async {
    var complete = Completer<bool>();
    Future<bool> received = complete.future;
    Socket s = await Socket.connect('192.168.4.1', 25443,
        timeout: const Duration(seconds: 2));
    s.listen((Uint8List data) {
      setState(() {
        bssid = String.fromCharCodes(data);
      });

      complete.complete(true);
    });
    s.write('uniqueid');
    await received;
    await s.close();
    return Future<bool>(() {
      return true;
    });
  }

  Future<bool> setConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    ip = await WiFiForIoTPlugin.getIP();
    if (ip != null) {
      return Future<bool>(() {
        if (connectivityResult == ConnectivityResult.wifi) {
          _isConnected = true;
        } else {
          _isConnected = false;
        }

        return true;
      });
    }
    return Future<bool>(() {
      return false;
    });
  }

  bool isFolderEmpty(String ssid) {
    List<FileSystemEntity> list = Directory(path).listSync();
    return list.isEmpty;
  }

  showAlert(BuildContext context, String perm) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Fonts.getText2('Uh Oh!'),
          content: Fonts.getText2(
              'In order to perform this action, WIBL needs access to your $perm! You can change this in your settings app'),
          actions: <Widget>[
            TextButton(
              child: Fonts.getText2('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        scaffoldMessengerKey: _messengerKey,
        title: 'Seabed 2030 Affiliate Logger Connector',
        theme: ThemeData(primarySwatch: secondary),
        home: Builder(builder: (BuildContext context) {
          return Scaffold(
              appBar: AppBar(
                title: const Text(
                  'Seabed 2030 Logger',
                  style: TextStyle(color: Colors.white, letterSpacing: .5),
                ),
                actions: [
                  IconButton(
                      onPressed: () {
                        //refresh ip address and SSID
                        setConnection();
                      },
                      icon: Icon(
                        Icons.refresh,
                        color: Colors.white,
                        size: displayWidth(context) * .04,
                      )),
                ],
              ),
              drawer: const SideBar(),
              backgroundColor: background,
              body: Column(children: [
                Icon(
                  Icons.wifi,
                  size: displayHeight(context) * .44,
                  color: Colors.white,
                ),
                FutureBuilder(
                    future: setConnection(),
                    builder:
                        (BuildContext context, AsyncSnapshot<bool> snapshot) {
                      return Text(
                        ip ?? 'Not connected',
                        style: TextStyle(
                            fontSize: displayWidth(context) / 10,
                            color: Colors.white,
                            letterSpacing: .5),
                      );
                    }),
                FutureBuilder(
                    future: setConnection(),
                    builder:
                        (BuildContext context, AsyncSnapshot<bool> snapshot) {
                      return Text(
                        bssid,
                        style: TextStyle(
                            fontSize: displayWidth(context) / 10,
                            color: Colors.white,
                            letterSpacing: .5),
                      );
                    }),
                const Spacer(),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                        height: displayHeight(context) / 8,
                        width: displayWidth(context) * .66,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18)),
                          ),
                          onPressed: () async {
                            Permission permission = Permission.location;
                            PermissionStatus status = await permission.status;
                            print(status);
                            if (status == PermissionStatus.restricted) {
                              showAlert(context, permission.toString());

                              status = await permission.status;

                              if (status != PermissionStatus.granted) {
                                //Only continue if permission granted
                                return;
                              }
                            }

                            if (status == PermissionStatus.denied) {
                              showAlert(context, permission.toString());
                              status = await permission.request();

                              if (status != PermissionStatus.granted) {
                                //Only continue if permission granted
                                return;
                              }
                            }
                            path = await _localPath;
                            if (_isConnected && ip!.startsWith('192.168.4.')) {
                              //check if folder still has files in it
                              await setId();
                              await _createFolder(bssid);
                              if (!isFolderEmpty(bssid)) {
                                _messengerKey.currentState!
                                    .showSnackBar(const SnackBar(
                                  content: Text("Folder is not Empty!"),
                                ));
                              } else {
                                Socket socket = await Socket.connect(
                                    '192.168.4.1', 25443,
                                    timeout: const Duration(seconds: 2));
                                fileCount = 0;
                                fileNums = [];
                                List<String> sizesString = [];
                                socket.listen((Uint8List data) {
                                  if (fileCount > 0) {
                                    List<String> temp =
                                        String.fromCharCodes(data).split('B');
                                    //SHOULD I NOT DO -1?
                                    for (int i = 0; i < temp.length - 1; i++) {
                                      sizesString.add(temp[i]);
                                    }
                                  }
                                  fileCount++;
                                });
                                socket.write('sizes');
                                await Future.delayed(
                                    const Duration(seconds: 10));

                                fileCount = 0;
                                for (int i = 0; i < sizesString.length; i++) {
                                  String str = sizesString[i];
                                  // print(str);
                                  // str =  str.substring(5);
                                  List<String> temp = str.split('.');
                                  temp = temp[1].split(' ');
                                  str = temp[0];
                                  fileCount++;
                                  fileNums.add(int.parse(str));
                                }
                                fileCount -= 1;
                                await socket.close();

                                Socket sock = await Socket.connect(
                                    '192.168.4.1', 25443,
                                    timeout: const Duration(seconds: 2));
                                var completeSize = Completer<bool>();
                                var completeData = Completer<bool>();

                                sock.listen((Uint8List data) {
                                  if (data.lengthInBytes == 4) {
                                    completeSize.complete(true);
                                  } else if (!completeSize.isCompleted) {
                                    createFile(data.sublist(4, data.length),
                                        fileNum++);
                                    completeSize.complete(true);
                                    completeData.complete(true);
                                  } else {
                                    createFile(data, fileNum++);
                                    completeData.complete(true);
                                  }
                                });
                                for (int i = 0; i < fileNums.length; i++) {
                                  int tran = fileNums[i];
                                  completeSize = Completer<bool>();
                                  completeData = Completer<bool>();
                                  Future<bool> receivedSize =
                                      completeSize.future;
                                  Future<bool> receivedData =
                                      completeData.future;
                                  sock.write('transfer $tran');
                                  await receivedSize;
                                  await receivedData;
                                }
                                await sock.close();
                                print('done transferring');
                              }
                            } else if (_isConnected) {
                              //connected to a non-wibl network
                              _messengerKey.currentState!
                                  .showSnackBar(const SnackBar(
                                content: Text(
                                    "You are not connected to a wibl network"),
                              ));
                            } else {
                              _messengerKey.currentState!
                                  .showSnackBar(const SnackBar(
                                content:
                                    Text("You are not connected to a network"),
                              ));
                            }
                          },
                          child: const Text(
                            'Transfer Data',
                            style: TextStyle(
                                fontSize: 36,
                                color: Colors.white,
                                letterSpacing: .5),
                            textAlign: TextAlign.center,
                          ),
                        ))),
                const Spacer(),
              ]));
        }));
  }
}
