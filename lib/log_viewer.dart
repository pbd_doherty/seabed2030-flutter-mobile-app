import 'package:flutter/material.dart';
import 'colors.dart';
import 'drawer.dart';
import 'fonts.dart';


void main() => runApp(const LogViewer());

class LogViewer extends StatelessWidget {
  const LogViewer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'LogViewer',
      theme: ThemeData(primarySwatch: secondary),
      home: Scaffold(
        appBar: AppBar(
          title: Fonts.getText1('Log Viewer'),
        ),
        backgroundColor: background,
        drawer: const SideBar(),
      ),
    );
  }
}