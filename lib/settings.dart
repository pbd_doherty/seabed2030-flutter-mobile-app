import 'package:flutter/material.dart';
import 'colors.dart';
import 'drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'fonts.dart';

void main() => runApp(const Settings());

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  int scanValue = 5, entriesShown = 0, debugLevel = 1;
  var _bluetooth, _aws, _demo;




  void loadCounter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _bluetooth = prefs.getBool('_bluetooth') ?? true;
      _aws = prefs.getBool('_aws') ?? true;
      _demo = prefs.getBool('_demo') ?? false;
    });
  }

  @override
  void initState() {
    super.initState();
    loadCounter();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Settings',
      theme: ThemeData(primarySwatch: secondary),
      home: Scaffold(
        appBar: AppBar(
          title: Fonts.getText1('Settings'),
        ),
        backgroundColor: background,
        body: ListView(children: [
           ListTile(
            title: Fonts.getText1('General Settings'),
          ),
          SwitchListTile(
            activeColor: secondary,
            value: _bluetooth ?? true,
            title:Fonts.getText1('Show Bluetooth Transactions'),
            secondary: const Icon(Icons.bluetooth, color: Colors.black),
            subtitle: const Text('Shows GATT Listings and BT messages in Logger View'),
            onChanged: (value) async {
              setState(() {
                _bluetooth = value;

              });
              SharedPreferences p = await _prefs;
              p.setBool('_bluetooth', _bluetooth);
            },
          ),
          ListTile(
              leading: const Icon(Icons.access_time, color: Colors.black),
              title: Fonts.getText1('Modify Scan Length...'),
              subtitle: const Text('Modifies the scan time from 5 seconds to '
                  '30 seconds. Warning, increasing scan times will use'
                  'more battery!'),
              onTap: () {
                showDialog<void>(
                    context: context,
                    builder: (context) {
                      return StatefulBuilder(
                        builder: (context, setState) {
                          return AlertDialog(
                            title:Fonts.getText2('Modify Scan Length'),
                            content: SizedBox(
                                width: 300,
                                height: 250,
                                child: ListView(
                                  children: [
                                    RadioListTile(
                                      title: Fonts.getText2('5 Seconds'),
                                      value: 5,
                                      groupValue: scanValue,
                                      onChanged: (value) {

                                        scanValue = value as int;
                                       // _prefs.setInt('_scan',_scan_value);
                                        Navigator.pop(context);
                                      },
                                    ),
                                    RadioListTile(
                                      title: Fonts.getText2('10 Seconds'),
                                      value: 10,
                                      groupValue: scanValue,
                                      onChanged: (value) {
                                        scanValue = value as int;
                                        Navigator.pop(context);
                                      },
                                    ),
                                    RadioListTile(
                                      title: Fonts.getText2('15 Seconds'),
                                      value: 15,
                                      groupValue: scanValue,
                                      onChanged: (value) {
                                        scanValue = value as int;
                                        Navigator.pop(context);
                                      },
                                    ),
                                    RadioListTile(
                                      title: Fonts.getText2('30 Seconds'),
                                      value: 30,
                                      groupValue: scanValue,
                                      onChanged: (value) {
                                        scanValue = value as int;
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                )),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                textColor: teal,
                                child: Fonts.getText2('Close'),
                              ),
                            ],
                          );
                        },
                      );
                    });
              }),
           ListTile(
            title: Fonts.getText1('AWS Settings'),
          ),
          SwitchListTile(
            value: _aws ?? true,
            secondary:
                const Icon(Icons.cloud_upload_rounded, color: Colors.black),
            activeColor: secondary,
            title: Fonts.getText1('AWS Enabler'),
            subtitle: const Text('If on, items will be sent to the S3 bucket '
                'normally'),
            onChanged: (value)async {
              setState(() {
                _aws = value;
              });
              SharedPreferences p = await _prefs;
              p.setBool('_aws', _aws);
            },
          ),
           ListTile(
            // leading:Icon(Icons.developer_board_rounded,
            //    color: Colors.black),
            title: Fonts.getText1('Developer Settings'),
          ),
          ListTile(
            leading: const Icon(Icons.usb, color: Colors.black),
            title: Fonts.getText1('Select Debug Level...'),
            subtitle: const Text('Prints debug messages to the log Views '
                'and command line'),
            onTap: () {
              showDialog<void>(
                  context: context,
                  builder: (context) {
                    return StatefulBuilder(
                      builder: (context, setState) {
                        return AlertDialog(
                            title: Fonts.getText2('Select Debug Level'),
                          content: SizedBox(
                              width: 300,
                              height: 250,
                              child: ListView(
                                children: [
                                  RadioListTile(
                                    title: Fonts.getText2('No Debug'),
                                    value: 0,
                                    groupValue: debugLevel,
                                    onChanged: (value) {
                                      debugLevel = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                  RadioListTile(
                                    title: Fonts.getText2('Basic'),
                                    value: 1,
                                    groupValue: debugLevel,
                                    onChanged: (value) {
                                      debugLevel = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                  RadioListTile(
                                    title: Fonts.getText2('Enhanced'),
                                    value: 2,
                                    groupValue: debugLevel,
                                    onChanged: (value) {
                                      debugLevel = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                ],
                              )),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              textColor: teal,
                              child: Fonts.getText2('Close'),
                            ),
                          ],
                        );
                      },
                    );
                  });
            },
          ),
          SwitchListTile(
            value: _demo ?? false,
            secondary: const Icon(Icons.settings_input_component_rounded,
                color: Colors.black),
            title: Fonts.getText1('Demo Mode'),
            activeColor: secondary,
            subtitle: const Text('Fudges some data'),
            onChanged: (value) async{
              setState(() {
                _demo = value;
              });
              SharedPreferences p = await _prefs;
              p.setBool('_demo', _demo);

            },
          ),
          ListTile(
            leading: const Icon(Icons.list, color: Colors.black),
            title: Fonts.getText1('Log Entries Shown'),
            subtitle: const Text('The total number of log entries to show at a time'),
            onTap: () {
              showDialog<void>(
                  context: context,
                  builder: (context) {
                    return StatefulBuilder(
                      builder: (context, setState) {
                        return AlertDialog(
                          title: Fonts.getText2('Log Entries Shown'),
                          content: SizedBox(
                              width: 300,
                              height: 250,
                              child: ListView(
                                children: [
                                  RadioListTile(
                                    title: Fonts.getText2('20 Items'),
                                    value: 0,
                                    groupValue: entriesShown,
                                    onChanged: (value) {
                                      entriesShown = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                  RadioListTile(
                                    title: Fonts.getText2('30 Items'),
                                    value: 1,
                                    groupValue: entriesShown,
                                    onChanged: (value) {
                                      entriesShown = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                  RadioListTile(
                                    title: Fonts.getText2('100 Items'),
                                    value: 2,
                                    groupValue: entriesShown,
                                    onChanged: (value) {
                                      entriesShown = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                  RadioListTile(
                                    title: Fonts.getText2('Everything'),
                                    value: 3,
                                    groupValue: entriesShown,
                                    onChanged: (value) {
                                      entriesShown = value as int;
                                      Navigator.pop(context);
                                    },
                                  ),
                                ],
                              )),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              textColor: teal,
                              child: Fonts.getText2('Close'),
                            ),
                          ],
                        );
                      },
                    );
                  });
            },
          ),
        ]),
        drawer: const SideBar(),
      ),
    );
  }
}
