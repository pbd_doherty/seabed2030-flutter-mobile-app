import 'package:cross_platform_test/colors.dart';
import 'package:flutter/material.dart';
import 'folders.dart';
import 'fonts.dart';
import 'log_viewer.dart';
import 'main.dart';
import 'settings.dart';

class SideBar extends StatelessWidget {
  const SideBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      backgroundColor: background,
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: secondary,
                image: DecorationImage(
                    image:
                    AssetImage('res/mipmap/ccom_logo.png'),
                  scale: 0.5,
                    )),
            child: Container(
              child:Fonts.getText1('Seabed 2030 Affiliate Logger Connector Version 0.20.1 - TUNA'),
              alignment: Alignment.bottomCenter,
              height: 10,
            ),
          ),
          ListTile(
            leading: const Icon(Icons.bluetooth),
            title: Fonts.getText1('Logger Connections'),
            onTap: () {
               Navigator.pop(context);
               Navigator.push(context, MaterialPageRoute(builder: (context) => const AppScreen()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.folder),
            title: Fonts.getText1('Folders'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const Folders()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.list),
            title: Fonts.getText1('Log Viewer'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const LogViewer()));
            },
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: Fonts.getText1('Settings'),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Settings()),
              );
            },
          ),
        ],
      ),
    );
  }

}