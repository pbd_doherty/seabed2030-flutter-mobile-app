import 'package:cross_platform_test/colors.dart';
import 'package:flutter/material.dart';
//import 'package:google_fonts/google_fonts.dart';

class Fonts {


    static Text getText1(String content){
      return Text(content,
         style:    const TextStyle(color: Colors.white, letterSpacing: .5));
          // GoogleFonts.staatliches(
        //   textStyle:
        //   const TextStyle(color: Colors.white, letterSpacing: .5),
        // ),);
    }

    static Text getText2(String content){
      return Text(content,
        style: const TextStyle(color: secondary, letterSpacing: .5));
        // GoogleFonts.staatliches(
        //   textStyle:
        //   const TextStyle(color: secondary, letterSpacing: .5),
        // ),);
    }
}
