import 'dart:io' as io;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:cross_platform_test/colors.dart';
import 'package:cross_platform_test/sizes.dart';
import 'package:flutter/material.dart';

//import 'package:google_fonts/google_fonts.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:uuid/uuid.dart';
import 'drawer.dart';
import 'colors.dart';
import 'files.dart';
import 'fonts.dart';
import 'dart:convert';

//import 'dart:typed_data';
import 'package:http/http.dart' as http;
import 'package:amazon_cognito_identity_dart/sig_v4.dart';
import 'Policy.dart';

void main() {
  runApp(
    const MaterialApp(
      home: Folders(),
    ),
  );
}

class Folders extends StatefulWidget {
  const Folders({Key? key}) : super(key: key);

  @override
  Folder createState() => Folder();
}

class Folder extends State<Folders> {
  final _messengerKey = GlobalKey<ScaffoldMessengerState>();
  List<io.FileSystemEntity> folders = [];
  List<String> folderNames = [];
  int numFolders = 0;
  String path = '';

  //AWS VARIABLES
  //TO CHANGE WITH SHARED PREFERENCES
  String accessKeyId = 'AKIA2VY2CT2EPBUANS7H';
  String secretKeyId = 'zlabRD/gwWwnIlAJlAzDv7y1Bmk3wMwQ/Q0mFq6K';
  String region = 'us-east-1';
  String bucketname = "wibl-dev";
  String s3Endpoint = 'https://wibl-dev.s3.amazonaws.com';

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    path = directory.path;
    return directory.path;
  }

  Future<List<String>> getFolders() async {
    String path = await _localPath;
    return Future<List<String>>(() {
      folders = io.Directory(path).listSync();
      folderNames.clear();
      for (var element in folders) {
        setState(() {
          numFolders++;
          List<String> directories = element.path.split(r'/');
          String last = directories[directories.length - 1];

          folderNames.add(last);
        });
      }
      return folderNames;
    });
  }

  void uploadToAws() async {
    for (int i = 0; i < folderNames.length; i++) {
      List<io.FileSystemEntity> files =
          io.Directory(path + '/' + folderNames[i]).listSync();
      for (int j = 0; j < files.length; j++) {
        String temp = files[j].path;
        io.File file = io.File(temp);
        await uploadData(file);
        file.delete();
      }
    }
  }

  Future<String> uploadData(io.File f) async {
    final length = await f.length();
    var uuid = Uuid();

    String fileName = uuid.v4();
    const folderName = 'uploads';
    final uri = Uri.parse(s3Endpoint);
    final req = http.MultipartRequest("POST", uri);
    final multipartFile = http.MultipartFile(
        'file', http.ByteStream.fromBytes(await f.readAsBytes()), length,
        filename: fileName);

    final policy = Policy.fromS3PresignedPost(
        folderName + '/' + fileName, bucketname, accessKeyId, 15, length,
        region: region);
    final key =
        SigV4.calculateSigningKey(secretKeyId, policy.datetime, region, 's3');
    final signature = SigV4.calculateSignature(key, policy.encode());

    req.files.add(multipartFile);
    req.fields['key'] = policy.key;
    req.fields['acl'] = 'public-read';
    req.fields['X-Amz-Credential'] = policy.credential;
    req.fields['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
    req.fields['X-Amz-Date'] = policy.datetime;
    req.fields['Policy'] = policy.encode();
    req.fields['X-Amz-Signature'] = signature;

    try {
      final res = await req.send();
      await for (var value in res.stream.transform(utf8.decoder)) {
        return Future<String>(() {
          return value;
        });
      }
    } catch (e) {
      return Future<String>(() {
        return e.toString();
      });
    }
    return Future<String>(() {
      return '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        scaffoldMessengerKey: _messengerKey,
        title: 'Folders: $numFolders',
        theme: ThemeData(primarySwatch: secondary),
        home: Builder(builder: (BuildContext context) {
          return Scaffold(
              appBar: AppBar(
                title: Fonts.getText1('Folders'),
                actions: [
                  IconButton(
                      onPressed: () {
                        showAlert(context);
                      },
                      icon: Icon(
                        Icons.delete,
                        color: Colors.white,
                        size: displayWidth(context) * .04,
                      )),
                ],
              ),
              drawer: const SideBar(),
              backgroundColor: background,
              body: Column(
                children: [
                  Expanded(
                    child: FutureBuilder(
                        future: getFolders(),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<String>> snapshot) {
                          return ListView.builder(
                            itemCount: folderNames.length,
                            itemBuilder: (context, index) {
                              return ListTile(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Files(
                                              p: folderNames[index],
                                            )),
                                  );
                                },
                                title: Text(
                                  folderNames[index],
                                  style: const TextStyle(
                                      color: Colors.white, letterSpacing: .5),
                                  textAlign: TextAlign.center,
                                ),
                              );
                            },
                          );
                        }),
                  ),
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: SizedBox(
                          height: displayHeight(context) / 8,
                          width: displayWidth(context) * .66,
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18)),
                            ),
                            onPressed: () async {
                              //check if connected to wifi or cellular
                              Permission permission = Permission.location;
                              PermissionStatus status = await permission.status;
                              print(status);
                              if (status == PermissionStatus.restricted) {
                                showAlert2(context, permission.toString());

                                status = await permission.status;

                                if (status != PermissionStatus.granted) {
                                  //Only continue if permission granted
                                  return;
                                }
                              }

                              if (status == PermissionStatus.denied) {
                                showAlert2(context, permission.toString());
                                status = await permission.request();

                                if (status != PermissionStatus.granted) {
                                  //Only continue if permission granted
                                  return;
                                }
                              }

                              var connectivityResult =
                                  await (Connectivity().checkConnectivity());
                              if (connectivityResult ==
                                      ConnectivityResult.wifi ||
                                  connectivityResult ==
                                      ConnectivityResult.mobile) {
                                uploadToAws();
                              } else {
                                _messengerKey.currentState!
                                    .showSnackBar(const SnackBar(
                                  content:
                                      Text("Not connected to Wifi or LTE!"),
                                ));
                              }
                            },
                            child: const Text(
                              'Upload all Files',
                              style: TextStyle(
                                  fontSize: 36,
                                  color: Colors.white,
                                  letterSpacing: .5),
                              textAlign: TextAlign.center,
                            ),
                          ))),
                  const Spacer()
                ],
              ));
        }));
  }

  showAlert(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Fonts.getText2('Hold Up!'),
          content: Fonts.getText2(
              'This will delete all wibl folders from this device! All data will be lost. Are you sure you wish to continue?'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Fonts.getText2('Cancel'),
            ),
            TextButton(
              child: Fonts.getText2('Yes'),
              onPressed: () {
                Navigator.of(context).pop();
                deleteAllFolders();
              },
            ),
          ],
        );
      },
    );
  }

  showAlert2(BuildContext context, String perm) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Fonts.getText2('Uh Oh!'),
          content: Fonts.getText2(
              'In order to perform this action, WIBL needs access to your $perm! You can change this in your settings app'),
          actions: <Widget>[
            TextButton(
              child: Fonts.getText2('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void deleteAllFolders() async {
    final directory = await getApplicationDocumentsDirectory();
    if (directory.existsSync()) {
      directory.deleteSync(recursive: true);
      setState(() {
        folderNames.clear();
      });
    }
  }
}
