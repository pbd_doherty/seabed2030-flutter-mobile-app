import 'dart:io' as io;

import 'package:cross_platform_test/colors.dart';
import 'package:cross_platform_test/sizes.dart';
import 'package:flutter/material.dart';
//import 'package:google_fonts/google_fonts.dart';
import 'package:path_provider/path_provider.dart';
import 'colors.dart';
import 'folders.dart';
import 'fonts.dart';


void main() {
  runApp(
    const MaterialApp(
      home: Files(p: '',),
    ),
  );
}

class Files extends StatefulWidget {
  final String p;
  const Files({Key? key, required this.p}) : super(key: key);

  @override
  File createState() => File();
}

class File extends State<Files> {
  final _messengerKey = GlobalKey<ScaffoldMessengerState>();
  List<io.FileSystemEntity> files = [];
  List<String> fileNames = [];
  int numFiles = 0;
  String path  = '';
  String addition = '';
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<List<String>> getFiles() async {
    String path = await _localPath;
    String ssid = widget.p;
    path += '/$ssid';

    return Future<List<String>> ((){
      files = io.Directory(path).listSync();
      fileNames.clear();
      for (var element in files) {
        setState(() {
          numFiles++;
          List<String> directories = element.path.split(r'/');
          String last = directories[directories.length - 1];
          fileNames.add(last);
        });
      }
      return fileNames;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        scaffoldMessengerKey: _messengerKey,
        title: widget.p,
        theme: ThemeData(primarySwatch: secondary),
        home: Builder(builder: (BuildContext context) {
          return Scaffold(
              appBar: AppBar(
                title: Fonts.getText1(widget.p),
                actions: [
                  IconButton(
                      onPressed: ()  {
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const Folders()),
                        );
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.white,
                        size: displayWidth(context) * .04,
                      )),
                ],
              ),
              backgroundColor: background,
              body:FutureBuilder(future: getFiles() ,builder:(BuildContext context, AsyncSnapshot<List<String>> snapshot) {
               return ListView.builder(
                  itemCount: fileNames.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () {
                        if(addition == ''){
                          setState(() {
                            addition = fileNames[index];
                          });
                        }
                      },
                      title: Text(
                        fileNames[index],
                        style: const TextStyle(color: Colors.white, letterSpacing: .5),
                        // GoogleFonts.staatliches(
                        //   textStyle: const TextStyle(
                        //       color: Colors.white, letterSpacing: .5),
                        // ),
                        textAlign: TextAlign.center,
                      ),
                    );
                  },
                );
              }),

              );
        }));


  }


}
